from distutils.util import strtobool

import bitmex_lib
import util
import configparser
import logging
import threading
import os
from flask_basicauth import BasicAuth
import urllib
from time import sleep
from urllib.parse import urlparse
import websocket
import socket
import json
from flask import Flask, json, Response, render_template
from flask_restful import Api, Resource, reqparse
import time
import requests
import bot.payload.payload as payload

# Configuring Logging
logging.basicConfig(
    level=logging.INFO,
    format='%(asctime)s - %(levelname)s - %(message)s'
)


class Signal(util.ToStringAndToJson):
    signal_type = 'LONG'
    state = 'ENTER'
    symbol = ''
    signal_time = 0

    def __init__(self, data):
        signal_type = data.get('command')
        if signal_type:
            if signal_type == 'EXIT_LONG':
                self.signal_type = "short"
            elif signal_type == 'EXIT_SHORT':
                self.signal_type = "long"
            elif signal_type == 'ENTER_SHORT':
                self.signal_type = "short"
            elif signal_type == 'ENTER_LONG':
                self.signal_type = "long"
        self.entry_price = float(data.get('price', 0.0))
        self.exit_price = float(data.get('price', 0.0))
        self.symbol = data.get('symbol', self.symbol)
        self.signal_time = time.time()
        self.entry_order = None
        self.exit_order = None
        self.stoploss_order = None
        self.state = ''

    def is_long(self):
        return self.signal_type == 'long'

    def is_short(self):
        return self.signal_type == 'short'

    def to_json(self):
        return {"signal_type": self.signal_type,
                "price": self.entry_price,
                "symbol": self.symbol,
                "signal_time": self.signal_time}


class BitmexBotConfig(util.ToStringAndToJson):

    def __init__(self, data):
        self.max_equity = float(data.get('max_equity', 1))
        self.leverage = float(data.get('leverage', 3))
        self.entry = data.get('entry', "LIMIT")
        self.exit = data.get('exit', "LIMIT")
        self.entry_stay_threshold = float(data.get('entry_stay_threshold', 20))
        self.entry_adjustment = float(data.get('entry_adjustment', 0.1))
        self.target_adjustment = float(data.get('target_adjustment', 1))
        self.trailing_stop_mode = True if (str(data.get('trailing_stop_mode', 'true'))) == 'true' else False
        self.trailing_stop_amount = float(data.get('trailing_stop_amount', 2))
        self.stop_loss = True if str(data.get('stop_loss', 'true')) == 'true' else False
        self.stop_loss_value = float(data.get('stop_loss_value', 2))
        self.stop_loss_trigger = data.get('stop_loss_trigger', 'LAST_PRICE')
        self.api_key = data.get('api_key',
                                'CQB_12kQjzNce3h764J6ZOkA')  # 0qZLtKZe1sVl79XZxF8Q2TGR CQB_12kQjzNce3h764J6ZOkA
        self.api_secret = data.get('api_secret',
                                   '0Yiq0TLoL0plGyRua3JJ6i2BylyEICU6jr4sf-uWjfqPUnY-')  # DYxFWQrctsBoXXtTzYc27y6iietMtJIgFfUfKfW51hi_mXT0 0Yiq0TLoL0plGyRua3JJ6i2BylyEICU6jr4sf-uWjfqPUnY-
        self.mode = data.get('mode', 'DEMO')

    def update(self, data):
        self.__init__(data)

    def to_json(self):
        return {'max_equity': self.max_equity,
                'leverage': self.leverage,
                'entry': self.entry,
                'exit': self.exit,
                'entry_stay_threshold': self.entry_stay_threshold,
                'entry_adjustment': self.entry_adjustment,
                'target_adjustment': self.target_adjustment,
                'trailing_stop_mode': str(self.trailing_stop_mode).lower(),
                'trailing_stop_amount': self.trailing_stop_amount,
                'stop_loss': str(self.stop_loss).lower(),
                'stop_loss_value': self.stop_loss_value,
                'stop_loss_trigger': self.stop_loss_trigger,
                'api_key': self.api_key,
                'api_secret': self.api_secret,
                'mode': self.mode}

    def get_connect_url(self):
        if self.mode == 'DEMO':
            return 'https://testnet.bitmex.com/api/v1/'
        else:
            return 'https://www.bitmex.com/api/v1/'


class BitmexBot:
    symbol = 'XBTUSD'

    def __init__(self, config):
        self.config = config
        self.bitmex_url = config.get_connect_url()
        self.client = bitmex_lib.BitMEX(base_url=self.bitmex_url,
                                        symbol='XBTUSD',
                                        apiKey=config.api_key,
                                        apiSecret=config.api_secret)
        self.ticker = {}
        self.data = {}
        self.keys = {}
        self.ws = None
        self.client.change_leverage(config.leverage)
        self.short_signal = None
        self.long_signal = None
        self.exited = False
        self.payload = payload.Payload(self.consume_signal)
        self.payload.start()

    def consume_signal(self, body=None, data=None):
        if data is None:
            data = json.loads(body.decode('utf8'))
        signal = Signal(data)
        logging.info("Consuming SIGNAL {}".format(signal.to_string()))

        signal.entry_price = self.round_price(signal.entry_price)
        signal.exit_price = self.round_price(signal.exit_price)
        logging.info(json.dumps(data))
        if signal.is_long() and self.ws.sock and self.ws.sock.connected and self.short_signal is None:
            if self.long_signal is None:
                return self.consume_long_enter(signal)
            else:
                self.long_signal.exit_price = signal.exit_price
                self.long_signal.state = 'UPDATE'
                logging.info("Signal Long - skipping signal and update exit price to {}".format(signal.exit_price))
                return "Skipping signal and update exit price"
        elif signal.is_short() and self.ws.sock and self.ws.sock.connected and self.long_signal is None:
            if self.short_signal is None:
                return self.consume_short_enter(signal)
            else:
                self.short_signal.exit_price = signal.exit_price
                self.short_signal.state = 'UPDATE'
                logging.info("SignalShort - Skipping signal and update exit price to {}".format(signal.exit_price))
                return "Skipping signal and update exit price"
    """
    Enter long position and exit with short
    """

    def consume_long_enter(self, signal):
        logging.info("Consuming LONG ENTER {}".format(signal.to_string()))
        try:
            is_limit = self.config.entry == 'LIMIT'
            next_price = self.get_next_price(signal)
            threshold = self.get_threshold(signal, next_price)
            if not is_limit or threshold > next_price:
                order = {
                    'orderQty': self.config.max_equity,
                    'symbol': signal.symbol,
                    'side': 'Buy',
                    'ordType': 'Limit' if is_limit else 'Market',
                    'clOrdID': self.client.gen_order_id()
                }
                if is_limit:
                    order['price'] = next_price
                order = self.client.place_order_raw(order)
                signal.entry_order = order
                signal.state = 'ENTER'
                self.long_signal = signal
                return "OK"
            else:
                m = "Enter price is no longer valid, threshold={:.2f}, next price={:.2f}, " \
                    "bid={:.2f}, ask={:.2f}".format(threshold, next_price, self.ticker['bidPrice'],
                                                    self.ticker['askPrice'])
                logging.info(m)
                return m

        except Exception as e:
            self.long_signal = None
            logging.error("Error while consuming LONG")
            logging.exception(e)
            return "Error while consuming LONG"

    """
    Enter short position and exit with long
    """

    def consume_short_enter(self, signal):
        logging.info("Consuming SHORT")
        try:
            is_limit = self.config.entry == 'LIMIT'
            next_price = self.get_next_price(signal)
            threshold = self.get_threshold(signal, next_price)
            if not is_limit or threshold < next_price:
                order = {
                    'orderQty': self.config.max_equity,
                    'symbol': signal.symbol,
                    'side': 'Sell',
                    'ordType': 'Limit' if is_limit else 'Market',
                    'clOrdID': self.client.gen_order_id(),
                }
                if is_limit:
                    order['price'] = next_price
                order = self.client.place_order_raw(order)
                signal.entry_order = order
                signal.state = 'ENTER'
                self.short_signal = signal
                return "OK"
            else:
                m = "Enter price is no longer valid, threshold={:.2f}, next price={:.2f}" \
                    "bid={:.2f}, ask={:.2f}".format(threshold, next_price, self.ticker['bidPrice'],
                                                    self.ticker['askPrice'])
                logging.info(m)
                return m

        except Exception as e:
            self.long_signal = None
            logging.error("Error while consuming LONG")
            logging.exception(e)
            return "Error while consuming LONG"

    def get_stoploss_price(self, signal):

        roe = self.config.stop_loss_value * 0.01
        entry_price = signal.entry_order['avgPx']
        qty = signal.entry_order['cumQty']

        order_value_xbt = 0.000001 * entry_price * qty
        taker_fees_2 = order_value_xbt * (2 * 0.00075)
        be_value = order_value_xbt + taker_fees_2 if signal.is_long() else order_value_xbt - taker_fees_2
        be_value = be_value / 0.000001 / qty

        return self.round_price(be_value - (be_value * roe) / self.config.leverage if signal.is_long() else be_value + (
                    be_value * roe) / self.config.leverage)

    def get_trailingstop_price(self, signal):

        entry_price = signal.entry_order['avgPx']
        qty = signal.entry_order['cumQty']

        order_value_xbt = 0.000001 * entry_price * qty
        taker_fees_2 = order_value_xbt * (2 * 0.00075)
        be_value = order_value_xbt + taker_fees_2 if signal.is_long() else order_value_xbt - taker_fees_2
        be_value = be_value / 0.000001 / qty

        return self.round_price(be_value + self.config.trailing_stop_amount if signal.is_long() else be_value - self.config.trailing_stop_amount)

    def check_orders(self, signal):

        is_limit = self.config.exit == 'LIMIT'
        is_trailing = self.config.exit == 'TRAILING'
        if signal.entry_order['ordStatus'] == 'Canceled' or signal.entry_order['ordStatus'] == 'Rejected':
            if signal.signal_type == 'long':
                self.long_signal = None
            elif signal.signal_type == 'short':
                self.short_signal = None
            logging.info("Removing canceled order: {}".format(signal.entry_order['orderID']))
        elif signal.entry_order['ordStatus'] == 'Filled':
            if signal.exit_order and signal.exit_order['ordStatus'] == 'Filled':
                if signal.stoploss_order:
                    self.client.cancel(signal.stoploss_order['orderID'])
                if signal.signal_type == 'long':
                    self.long_signal = None
                elif signal.signal_type == 'short':
                    self.short_signal = None
                logging.info("The signal is executed on exit orders")
                return
            elif signal.stoploss_order and signal.stoploss_order['ordStatus'] == 'Filled':
                if signal.exit_order:
                    self.client.cancel(signal.exit_order['orderID'])
                if signal.signal_type == 'long':
                    self.long_signal = None
                elif signal.signal_type == 'short':
                    self.short_signal = None
                logging.info("The signal is executed on stoploss orders")
                return
            elif signal.exit_order and (
                    signal.exit_order['ordStatus'] == 'Rejected' or signal.exit_order['ordStatus'] == 'Canceled'):
                signal.exit_order = None
            elif signal.stoploss_order and signal.stoploss_order['ordStatus'] == 'Rejected':
                signal.stoploss_order = None
            elif signal.stoploss_order and signal.stoploss_order['ordStatus'] == 'Canceled':
                if signal.signal_type == 'long':
                    self.long_signal = None
                elif signal.signal_type == 'short':
                    self.short_signal = None
                return

            if signal.state == 'ENTER' or signal.state == 'UPDATE':
                if self.config.stop_loss and not signal.stoploss_order:
                    price = self.get_stoploss_price(signal)
                    order = {
                        'orderQty': signal.entry_order['cumQty'],
                        'symbol': signal.symbol,
                        'side': 'Sell' if signal.is_long() else 'Buy',
                        'ordType': 'Stop',
                        'stopPx': price,
                        'execInst': 'MarkPrice' if bot.config.stop_loss_trigger == 'MARK_PRICE' else 'LastPrice',
                        'clOrdID': self.client.gen_order_id(),
                    }
                    order = self.client.place_order_raw(order)
                    signal.stoploss_order = order
                    logging.info('Placed stoploss order at {}'.format(price))
                if is_limit and not signal.exit_order:
                    order = {
                        'ordType': 'Limit',
                        'side': 'Sell' if signal.is_long() else 'Buy',
                        'price': signal.exit_price,
                        'orderQty': signal.entry_order['cumQty'],
                        'symbol': signal.symbol,
                        'clOrdID': self.client.gen_order_id(),
                    }
                    order = self.client.place_order_raw(order)
                    signal.exit_order = order
                    logging.info('Placed exit order at {}'.format(signal.exit_price))
                if is_limit and signal.exit_order['price'] != signal.exit_price:
                    order = {'orderID': signal.exit_order['orderID'], 'price': signal.exit_price}
                    change = False
                    if signal.signal_type == 'long' and self.ticker['askPrice'] <= signal.exit_price:
                        change = True
                    elif signal.signal_type == 'short' and self.ticker['bidPrice'] >= signal.exit_price:
                        change = True
                    if change:
                        order = self.client.update_order_raw(order)
                        signal.exit_order['price'] = signal.exit_price
                        logging.info('Update exit order {}, to price {:.2f}'.format(str(order), signal.exit_price))
                if (signal.exit_order or is_trailing) and (signal.stoploss_order or not self.config.stop_loss):
                    signal.state = 'EXIT'
            if is_trailing and not signal.exit_order:

                current_price = self.ticker['bidPrice'] if signal.is_long() else self.ticker['askPrice']
                trailing_price_reached = False
                if signal.is_long():
                    trailing_price_reached = current_price >= self.get_trailingstop_price(signal)
                else:
                    trailing_price_reached = current_price <= self.get_trailingstop_price(signal)
                if trailing_price_reached:
                    order = {
                        'orderQty': signal.entry_order['cumQty'],
                        'ordType': 'Stop',
                        'symbol': signal.symbol,
                        'pegPriceType': 'TrailingStopPeg',
                        'pegOffsetValue': self.round_price(self.config.trailing_stop_amount) * (-1 if signal.is_long() else 1),
                        'side': 'Sell' if signal.is_long() else 'Buy',
                        'execInst': 'LastPrice',
                        'clOrdID': self.client.gen_order_id()
                    }
                    order = self.client.place_order_raw(order)
                    signal.exit_order = order
                    logging.info('Placed trailing exit stop order')
        elif self.config.entry == 'LIMIT':
            self.follow_price(signal)

    def follow_price(self, signal):

        next_price = self.get_next_price(signal)
        threshold = self.get_threshold(signal, next_price)
        if next_price:
            update = False
            if signal.is_long() and threshold >= next_price > signal.entry_order['price']:
                update = True
            elif signal.is_short() and threshold <= next_price < signal.entry_order['price']:
                update = True
            elif signal.is_long() and threshold < next_price or signal.is_short() and threshold > next_price:
                self.client.cancel(signal.entry_order['orderID'])
                if signal.signal_type == 'long':
                    self.long_signal = None
                elif signal.signal_type == 'short':
                    self.short_signal = None
                logging.info(
                    "Price went out of threshold, canceling entry order: {}".format(signal.entry_order['orderID']))
            if update:
                signal.last_update = time.time()
                order = {'orderID': signal.entry_order['orderID'], 'price': next_price}
                order = self.client.update_order_raw(order)
                signal.entry_order['price'] = next_price
                logging.info('Update entry order {}, to price {:.2f}'.format(str(order), next_price))

    def get_threshold(self, signal, next_price):

        threshold = self.config.entry_stay_threshold * 0.01
        if signal.signal_type == 'long':
            threshold = signal.entry_price + (next_price - signal.entry_price) * threshold
        elif signal.signal_type == 'short':
            threshold = signal.entry_price - (signal.entry_price - next_price) * threshold
        return self.round_price(threshold)

    def get_next_price(self, signal):

        adjustment = self.config.entry_adjustment
        next_price = None
        if signal.signal_type == 'long':
            next_price = self.ticker['bidPrice'] - adjustment
        elif signal.signal_type == 'short':
            next_price = self.ticker['askPrice'] + adjustment
        return self.round_price(next_price)

    def get_all_orders(self):

        orders = []
        if self.long_signal:
            if self.long_signal.entry_order:
                orders.append(self.long_signal.entry_order)
            if self.long_signal.exit_order:
                orders.append(self.long_signal.exit_order)
            if self.long_signal.stoploss_order:
                orders.append(self.long_signal.stoploss_order)
        if self.short_signal:
            if self.short_signal.entry_order:
                orders.append(self.short_signal.entry_order)
            if self.short_signal.exit_order:
                orders.append(self.short_signal.exit_order)
            if self.short_signal.stoploss_order:
                orders.append(self.short_signal.stoploss_order)
        return orders

    def get_all_order_ids(self, orders):

        order_ids = []
        for order in orders:
            order_ids.append(order['orderID'])
        return order_ids

    def start(self):

        self.exited = False
        self.wsURL = self.__get_url(self.bitmex_url)
        self.__connect(self.wsURL, self.symbol)

    def stop(self):
        self.exit_ws()

    def exit_ws(self):
        self.exited = True
        self.restart_ws()

    def restart_ws(self):
        if self.ws:
            try:
                self.ws.close()
            except Exception as e:
                logging.exception(e)

    def update_config(self, data):
        leverage = data.get('leverage')
        if leverage and self.config.leverage != leverage:
            try:
                logging.info("Changing leverage to {}".format(leverage))
                self.client.change_leverage(leverage)
            except Exception as e:
                logging.exception(e)
        self.config.update(data)

    def __get_url(self, base_url):
        symbol_subs = ["order", "position", "quote"]
        generic_subs = ["margin"]
        subscriptions = [sub + ':' + self.symbol for sub in symbol_subs]
        subscriptions += generic_subs
        url_parts = list(urllib.parse.urlparse(base_url))
        url_parts[0] = url_parts[0].replace('http', 'ws')
        url_parts[2] = "/realtime?subscribe={}".format(','.join(subscriptions))
        return urllib.parse.urlunparse(url_parts)

    def __connect(self, ws_url, symbol):

        logging.debug("Starting thread")
        self.ws = websocket.WebSocketApp(ws_url,
                                         on_message=self.__on_message,
                                         on_close=self.__on_close,
                                         on_open=self.__on_open,
                                         on_error=self.__on_error,
                                         header=self.__get_auth())
        self.wst = threading.Thread(target=self.ws.run_forever,
                                    kwargs=dict(ping_interval=10, ping_timeout=5,
                                                sockopt=((socket.IPPROTO_TCP, socket.TCP_NODELAY, 1),)))
        self.wst.daemon = False
        self.wst.start()
        logging.debug("Started thread")
        conn_timeout = 5
        while not self.ws.sock or not self.ws.sock.connected and conn_timeout:
            sleep(1)
            conn_timeout -= 1
        if not conn_timeout:
            logging.error("Couldn't connect to WS! Exiting.")

    def __on_error(self, error):

        if not self.exited:
            logging.error("Error : %s" % error)
            raise websocket.WebSocketException(error)

    def __on_open(self):

        logging.debug("Websocket Opened.")

    def __on_close(self):

        logging.info('Websocket Closed')
        if not self.exited:
            logging.info("Restarting Websocket")
            sleep(5)
            self.data = {}
            self.keys = {}
            self.start()
            logging.info("Restarted Websocket")

    def __get_auth(self):

        if self.config.api_key:
            logging.info("Authenticating with API Key.")
            # To auth to the WS using an API key, we generate a signature of a nonce and
            # the WS API endpoint.
            nonce = bitmex_lib.generate_nonce()
            return [
                "api-nonce: " + str(nonce),
                "api-signature: " + bitmex_lib.generate_signature(self.config.api_secret, 'GET', '/realtime', nonce,
                                                                  ''),
                "api-key:" + self.config.api_key
            ]
        else:
            logging.info("Not authenticating.")
            return []

    def findItemByKeys(self, keys, table, match_data):

        for item in table:
            matched = True
            for key in keys:
                if item[key] != match_data[key]:
                    matched = False
            if matched:
                return item

    def round_price(self, x):

        return round(0.5 * round(float(x) / 0.5), 2)

    def __update_orders(self, data, orders):

        for data_ord in data:
            for ord in orders:
                if data_ord['orderID'] == ord['orderID']:
                    ord.update(data_ord)

    def __on_message(self, message):
        try:
            message = json.loads(message)
            table = message['table'] if 'table' in message else None
            action = message['action'] if 'action' in message else None
            if action:
                if table not in self.data:
                    self.data[table] = []
                if action == 'partial':
                    if table != 'trade' and table != 'quote' and table != 'order':
                        self.keys[table] = message['keys']
                        self.data[table] += message['data']
                    elif table == 'quote':
                        self.ticker['bidPrice'] = message['data'][-1]['bidPrice']
                        self.ticker['askPrice'] = message['data'][-1]['askPrice']
                    elif table == 'order':
                        orders = self.get_all_orders()
                        order_ids = self.get_all_order_ids(orders)
                        if order_ids:
                            data_orders = self.client.get_orders(order_ids)
                            self.__update_orders(data_orders, orders)
                            logging.info('Updated orders status')
                elif action == 'insert':
                    if table == 'quote':
                        self.ticker['bidPrice'] = message['data'][-1]['bidPrice']
                        self.ticker['askPrice'] = message['data'][-1]['askPrice']
                    elif table == 'order':
                        orders = self.get_all_orders()
                        self.__update_orders(message['data'], orders)
                    else:
                        self.data[table] += message['data']
                elif action == 'update':
                    if table == 'order':
                        orders = self.get_all_orders()
                        self.__update_orders(message['data'], orders)
                    else:
                        for updateData in message['data']:
                            item = self.findItemByKeys(self.keys[table], self.data[table], updateData)
                            if item:
                                item.update(updateData)
                elif action == 'delete':
                    if table == 'order':
                        orders = self.get_all_orders()
                        self.__update_orders(message['data'], orders)
                    else:
                        for deleteData in message['data']:
                            item = self.findItemByKeys(self.keys[table], self.data[table], deleteData)
                            if item:
                                self.data[table].remove(item)
                if self.long_signal:
                    self.check_orders(self.long_signal)
                if self.short_signal:
                    self.check_orders(self.short_signal)

        except requests.exceptions.HTTPError as e:
            response = getattr(e, 'response', None)
            request = getattr(e, 'request', None)
            global bot
            if response is not None:
                if response.status_code == 401:
                    logging.error("API Key or Secret incorrect, please check and restart.")
                    logging.error("Error: " + response.text)
                    self.stop()
                    bot = None
                if response.status_code == 400:
                    ticker_data = response.json()
                    error = ticker_data['error']
                    message = error['message'].lower() if error else ''
                    if 'insufficient available balance' in message:
                        logging.error("Insufficient balance.")
                        self.stop()
                        bot = None
                    elif ('invalid ordstatus' in message or 'invalid orderid' in message) and request:
                        logging.error("Invalid ordStatus or invalid orderID.")
                        orders = self.get_all_orders()
                        order_ids = self.get_all_order_ids(orders)
                        if order_ids:
                            data_orders = self.client.get_orders(order_ids)
                            self.__update_orders(data_orders, orders)
                        logging.info('Order status update by request.')
                    else:
                        logging.error(str(e))
                elif response.status_code == 429:
                    logging.error("Ratelimited on current request. Sleeping, then trying again.")
                elif response.status_code == 503:
                    logging.error("Unable to contact the BitMEX API (503), retrying")
            else:
                logging.exception(e)
        except requests.exceptions.Timeout as e:
            logging.exception(e)
            logging.warning("Timed out on request")
        except requests.exceptions.ConnectionError as e:
            logging.exception(e)
            logging.warning("Unable to contact the BitMEX API. Please check the URL. Retrying.")
        except Exception as e:
            logging.exception(e)


class Rest(Resource):

    def post(self, op):

        global bot, unlock, unlock_time
        if unlock or time.time() > unlock_time:
            unlock = False
            unlock_time = time.time() + 7
            parser = reqparse.RequestParser(trim=True)
            if op != 'test_consume':
                parser.add_argument('max_equity')
                parser.add_argument('leverage')
                parser.add_argument('entry')
                parser.add_argument('exit')
                parser.add_argument('entry_adjustment')
                parser.add_argument('entry_follow_threshold')
                parser.add_argument('entry_stay_threshold')
                parser.add_argument('target_adjustment')
                parser.add_argument('trailing_stop_mode')
                parser.add_argument('trailing_stop_amount')
                parser.add_argument('dca_mode')
                parser.add_argument('dca_target')
                parser.add_argument('dca_target_multiplier')
                parser.add_argument('dca_target_times')
                parser.add_argument('stop_loss')
                parser.add_argument('stop_loss_value')
                parser.add_argument('stop_loss_trigger')
                parser.add_argument('api_key')
                parser.add_argument('api_secret')
                parser.add_argument('mode')
            else:
                parser.add_argument('command')
                parser.add_argument('price')
                parser.add_argument('symbol')
            args = parser.parse_args()
            if op == 'start':
                if bot:
                    unlock = True
                    return 'The bot is already running', 200
                elif args:
                    config.update(args)
                    bot = BitmexBot(config)
                    bot.start()
                    logging.info("Bot started")
                    unlock = True
                    return 'Ok', 200
                else:
                    unlock = True
                    return 'Invalid data in the request', 401
            elif op == 'stop':

                if bot:
                    bot.stop()
                    bot = None
                    logging.info("Bot stoped")
                    unlock = True
                    return 'Stoped bot', 200
                else:
                    unlock = True
                    return 'The bot is not started', 200
            elif op == 'update':

                if bot and args:
                    bot.update_config(args)
                    unlock = True
                    return 'Setting changed', 200
                else:
                    unlock = True
                    return 'The bot is not started', 200
            elif op == 'restart':
                if bot:
                    bot.restart_ws()
                    unlock = True
                    return 'Websocket restart', 200
                else:
                    unlock = True
                    return 'The bot is not started', 200
            elif op == 'test_consume':
                if bot and args:
                    unlock = True
                    return bot.consume_signal(None, args), 200
                else:
                    unlock = True
                    return 'Not consumed, start bot first'
            elif op == 'test_drop_signal':
                if bot:
                    bot.long_signal = None
                    bot.short_signal = None
                unlock = True
                return "OK", 200
        else:
            return 'Wait a bit', 200

    def get(self, op):

        global bot
        if op == 'settings':
            if bot:
                conf = config.to_json()
                if conf['mode'] == 'REAL':
                    conf['api_key'] = '*'
                    conf['api_secret'] = '*'
                data = json.dumps(conf)
                return Response(response=data, status=200, mimetype='application/json')
            else:
                data = json.dumps(config.to_json())
                return Response(response=data, status=200, mimetype='application/json')
        if op == 'test_signal':
            signal = {"action": "long",
                      "entry_price": "80",
                      "exit_price": "90",
                      "symbol": "ETHUSD"}
            signal = json.dumps(signal)
            return Response(response=signal, status=200, mimetype='application/json')


bot = None
unlock = True
unlock_time = 0
config = BitmexBotConfig({})

app = Flask(__name__, static_url_path='', static_folder='web/static', template_folder='web/templates')
app.config['BASIC_AUTH_USERNAME'] = 'samuel'
app.config['BASIC_AUTH_PASSWORD'] = 'bitmexbot'
app.config['BASIC_AUTH_FORCE'] = True
basic_auth = BasicAuth(app)


@app.route('/secret')
@basic_auth.required
def secret_view():
    return render_template('1secret.html')


api = Api(app)
api.add_resource(Rest, '/rest/<string:op>')
app.run(host='0.0.0.0', port=5000, debug=True)
