import json


class ToStringAndToJson:
    def to_json(self):
        return {}

    def to_string(self):
        return json.dumps(self.to_json())
