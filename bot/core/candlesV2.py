from bot import  settings
from datetime import datetime


def candles():



    cand = []
    g = []


    time = datetime.now()
    api_data_pull = settings.bitmex.kline(
        filters={"binSize": "5m", "symbol": "{}".format('XBTUSD'), 'endTime': '{}'.format(time), 'reverse': 'true',
                 'limit': '500'})[::-1]
    candle_data = [f for f in [d for d in [list(g.values()) for g in list(api_data_pull)]]]
    candles = [[q[0], q[2], q[3], q[4], q[5], q[7]] for q in candle_data]


    for f in candles:
        if float(datetime.strptime(f[0], '%Y-%m-%dT%H:%M:%S.%fZ').minute) % 15 != 0:
            g.append(f)

        if float(datetime.strptime(f[0], '%Y-%m-%dT%H:%M:%S.%fZ').minute) % 15 == 0:
            g.append(f)
            if len(g) == 1:
                cand.append(f)
                del g[:]

            elif len(g) == 2:
                f = [g[1][0], g[0][1], max(g[0][2], g[1][2]), min(g[0][3], g[1][3]), g[1][4], sum([g[0][-1], g[1][-1]])]
                cand.append(f)
                del g[:]

            elif len(g) == 3:

                f = [g[2][0],  # 0
                     g[0][1],  # Open 1
                     max(g[0][2], g[1][2], g[2][2]),  # High 2
                     min(g[0][3], g[1][3], g[2][3]),  # Low 3
                     g[2][4],  # Close 4
                     sum([g[0][-1], g[1][-1], g[2][-1]])]  # Volume 5

                cand.append(f)
                del g[:]
    # OHLC
    return [
        # [f[0] for f in cand],
        [f[1] for f in cand],
        [f[2] for f in cand],
        [f[3] for f in cand],
        [f[4] for f in cand]
    ]

#
# while True:
#     print()
#     print()
#     print()
#     print('+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++')
#     for d in candles():
#
#         # print(d)
#         settings.bitmex.quote_bucketed(filters={ "symbol": "{}".format('XBTUSD')})
#
#     import time
#     time.sleep(2)
#
