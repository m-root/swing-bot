from datetime import datetime
import pytz
import telegram
from bot.core import candle
import bot.settings as settings
import logging
from pyti import exponential_moving_average as ema, \
    relative_strength_index as rsi, \
    directional_indicators as di, \
    average_true_range as atr

from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.triggers.cron import CronTrigger

# Configuring Logging
logging.basicConfig(
    level=logging.INFO,
    format='%(asctime)s - %(levelname)s - %(message)s'
)
buy_list = []
sell_list = []


####################################
#              PAYLOAD             #
####################################

class Payload(object):

    def __init__(self, consume_func):
        self.consume_func = consume_func
        self.pair = None
        self.limit = None
        self.candle = None
        # OHLC
        self.pdi_data = None
        self.ndi_data = None
        self.ema_data = None
        self.rsi_data = None
        self.atr_data = None
        self.sched = None

    def start(self):
        self.sched = BackgroundScheduler()
        self.sched.add_job(lambda: self.logic(), CronTrigger.from_crontab('0,15,30,45 * * * *'))
        self.sched.start()

    def stop(self):
        if self.sched is not None:
            self.sched.shutdown()
            self.sched = None

    # -282619797
    def telegram_message(self, telegram_message):
        status = settings.bot.send_message(
            chat_id=settings.chat_id,
            text=telegram_message,
            parse_mode=telegram.ParseMode.HTML
        )
        return status

    def telegram_print(self):

        telegram_message = 'PAIR \t: {} \n' \
                           'RSI \t: {} \n' \
                           'ATR \t: {} \n' \
                           'PDI \t: {} \n' \
                           'NDI \t: {} \n'.format(

            self.pair,
            self.rsi_data[-6:-1],
            self.atr_data[-6:-1],
            self.pdi_data[-6:-1],
            self.ndi_data[-6:-1],
        )

        print(self.telegram_message(telegram_message))


    def on_screen_print(self):
        screen_print = 'PAIR \t: {} \n' \
                           'RSI \t: {} \n' \
                           'ATR \t: {} \n' \
                           'PDI \t: {} \n' \
                           'NDI \t: {} \n'.format(

            self.pair,
            self.rsi_data[-6:-1],
            self.atr_data[-6:-1],
            self.pdi_data[-6:-1],
            self.ndi_data[-6:-1],
        )

        print(screen_print)
        # print(self.telegram_message(screen_print))

    def buy_logic(self):

        if self.candle[-1][-2] > self.ema_data[-2] \
                and self.rsi_data[-2] > settings.buy_entry_rsi \
                and self.atr_data[-2] > settings.buy_entry_atr \
                and self.pdi_data[-2] > self.ndi_data[-2] \
                and self.pair not in buy_list:

            buy_list.append(self.pair)
            if self.pair in sell_list:
                sell_list.remove(self.pair)

            telegram_message = 'BITMEX SIGNAL : ENTER_SHORT : {} Price : {} Time {} Australia/Sydney time ' \
                .format(
                self.pair, float([quote_price for quote_price in self.candle[3]][-1]) ,
                str(datetime.now(pytz.timezone('Australia/Sydney'))
                    )
            )

            self.telegram_print()
            print(self.telegram_message(telegram_message))
            self.consume_func(data={
                "command": "ENTER_SHORT",
                # "order_id": 1,
                "symbol": "XBTUSD",
                "price": "{}".format(float([quote_price for quote_price in self.candle[3]][-1]))
            })
            return

        if self.candle[-1][-2] > self.ema_data[-2] \
                and self.rsi_data[-2] > settings.buy_entry_rsi \
                and self.atr_data[-2] > settings.buy_entry_atr \
                and self.pdi_data[-2] > self.ndi_data[-2]:

            print('We are currently long on {}'.format(self.pair))

            if self.pair not in buy_list:
                buy_list.append(self.pair)

                if self.pair in sell_list:
                    sell_list.remove(self.pair)

        self.buy_exit_logic()

        print(self.candle[-1][-2] > self.ema_data[-2] \
                and self.rsi_data[-2] > settings.buy_entry_rsi \
                and self.atr_data[-2] > settings.buy_entry_atr \
                and self.pdi_data[-2] > self.ndi_data[-2] \
                and self.pair not in buy_list)
        print()
        print('++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++')
        print('+++++++++++++++++++++++++++++++++               BUY  LOGIC               +++++++++++++++++++++++++++++++++')
        print('++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++')
        print()
        print('Price and EMA \t : \t{}, \t  Price \t :  {}, \t  EMA Price\t :  {}'.format((self.candle[-1][-2] > self.ema_data[-2]),self.candle[-1][-2] , self.ema_data[-2] ))
        print('RSI \t : \t{}, RSI \t :  {}, \t  RSI Settings \t :  {}'.format((self.rsi_data[-2] > settings.buy_entry_rsi), self.rsi_data[-2] , settings.buy_entry_rsi))
        print('ATR \t : \t {},\t  ATR \t :  {},\t ATR Settings \t :  {}'.format((self.atr_data[-2] > settings.buy_entry_atr), self.atr_data[-2], settings.buy_entry_atr))
        print('PDI > NDI \t : \t{},\t  PDI \t :  {} \t > \t  NDI \t :  {}'.format(self.pdi_data[-2] > self.ndi_data[-2], self.pdi_data[-2] , self.ndi_data[-2]))
        print('In BUY LIST \t : \t{}, \t  BUY LIST  \t :{} '.format(self.pair in buy_list, buy_list ))
        print()
        print('++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++')
        print()



    ####################################
    #          BUY EXIT LOGIC          #
    ####################################
    def buy_exit_logic(self):

        if self.candle[-1][-2] < self.ema_data[-2] \
                and self.pdi_data[-2] < self.ndi_data[-2] \
                and self.pair in buy_list:

            if self.pair in buy_list:
                buy_list.remove(self.pair)


            telegram_message = 'BITMEX SIGNAL : EXIT_SHORT : {} Price : {} Time {} Australia/Sydney time ' \
                .format(self.pair,
                        float([quote_price for quote_price in self.candle[3]][-1]),
                        str(datetime.now(pytz.timezone('Australia/Sydney'))
                            )
                        )

            self.telegram_print()
            del buy_list[:]
            print(self.telegram_message(telegram_message))

            self.consume_func(data={
                "command": "EXIT_SHORT",
                # "order_id": 1,
                "symbol": "XBTUSD",
                "price": "{}".format(float([quote_price for quote_price in self.candle[3]][-1]))
            })

            return

        print(self.candle[-1][-2] < self.ema_data[-2] \
                and self.pdi_data[-2] < self.ndi_data[-2] \
                and self.pair in buy_list)

    ####################################
    #          SELL ENTRY LOGIC        #
    ####################################
    def sell_logic(self):

        if self.candle[-1][-2] < self.ema_data[-2] \
                and self.rsi_data[-2] < settings.sell_entry_rsi \
                and self.atr_data[-2] > settings.sell_entry_atr \
                and self.pdi_data[-2] < self.ndi_data[-2] \
                and self.pair not in sell_list:

            sell_list.append(self.pair)
            if self.pair in buy_list:
                buy_list.remove(self.pair)

            telegram_message = 'BITMEX SIGNAL : ENTER_LONG : {} Price : {} Time {} Australia/Sydney time ' \
                .format(self.pair,
                        float([quote_price for quote_price in self.candle[3]][-1]),
                        str(datetime.now(pytz.timezone('Australia/Sydney'))
                            )
                        )

            self.telegram_print()
            print(self.telegram_message(telegram_message))
            # print(self.telegram_message())

            self.consume_func(data={
                    "command": "ENTER_LONG",
                    "symbol": "XBTUSD",
                    "price": "{}".format(float([quote_price for quote_price in self.candle[3]][-1]))
            })

            return

        if self.candle[-1][-2] < self.ema_data[-2] \
                and self.rsi_data[-2] < settings.sell_entry_rsi \
                and self.atr_data[-2] > settings.sell_entry_atr \
                and self.pdi_data[-2] < self.ndi_data[-2]:

            print('We are currently short on {}'.format(self.pair))

            if self.pair not in sell_list:
                sell_list.append(self.pair)

                if self.pair in buy_list:
                    buy_list.remove(self.pair)

        print(self.candle[-1][-2] < self.ema_data[-2] \
                and self.rsi_data[-2] < settings.sell_entry_rsi \
                and self.atr_data[-2] < settings.sell_entry_atr \
                and self.pdi_data[-2] < self.ndi_data[-2] \
                and self.pair not in sell_list)


        print()
        print('++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++')
        print('+++++++++++++++++++++++++++++++++               SELL  LOGIC               +++++++++++++++++++++++++++++++++')
        print('++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++')
        print()
        print('Price and EMA \t : \t{}, \t  Price \t :  {}, \t  EMA Price\t :  {}'.format((self.candle[-1][-2] < self.ema_data[-2]),self.candle[-1][-2] , self.ema_data[-2] ))
        print('RSI \t : \t{}, RSI \t :  {}, \t  RSI Settings \t :  {}'.format((self.rsi_data[-2] < settings.buy_entry_rsi), self.rsi_data[-2] , settings.buy_entry_rsi))
        print('ATR \t : \t {},\t  ATR \t :  {},\t ATR Settings \t :  {}'.format((self.atr_data[-2] > settings.buy_entry_atr), self.atr_data[-2], settings.buy_entry_atr))
        print('PDI < NDI \t : \t{},\t  PDI \t :  {} \t < \t  NDI \t :  {}'.format(self.pdi_data[-2] < self.ndi_data[-2], self.pdi_data[-2] , self.ndi_data[-2]))
        print('IN SELL LIST \t : \t{}, \t  SELL LIST  \t :{} '.format(self.pair in sell_list, sell_list ))
        print()
        print('++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++')
        print()


        self.sell_exit_logic()

    ####################################
    #          SELL EXIT LOGIC         #
    ####################################

    def sell_exit_logic(self):

        if self.candle[-1][-2] > self.ema_data[-2] \
                and self.pdi_data[-2] > self.ndi_data[-2] \
                and self.pair in sell_list:

            if self.pair in sell_list:
                sell_list.remove(self.pair)

            telegram_message = 'BITMEX SIGNAL : EXIT_LONG : {} Price : {} Time {} Australia/Sydney time ' \
                .format(self.pair,
                        float([quote_price for quote_price in self.candle[3]][-1]),
                        str(datetime.now(pytz.timezone('Australia/Sydney'))
                            )
                        )
            self.telegram_print()
            del sell_list[:]
            print(self.telegram_message(telegram_message))

            self.consume_func(data={
                "command": "EXIT_LONG",
                # "order_id": 1,
                "symbol": "XBTUSD",
                "price": "{}".format(float([quote_price for quote_price in self.candle[3]][-1]))
            })

            return

        print(self.candle[-1][-2] > self.ema_data[-2] \
                and self.pdi_data[-2] > self.ndi_data[-2] \
                and self.pair in sell_list)

    def logic(self):
        logging.info("{} Invoking logic".format(datetime.utcnow()))
        self.pair = 'BTC/USD'
        self.limit = settings.limit
        self.candle = candle.candles()
        # OHLC
        self.pdi_data = di.positive_directional_index(
            close_data=self.candle[3],
            high_data=self.candle[1],
            low_data=self.candle[2],
            period=settings.pdi_period
        )

        self.ndi_data = di.negative_directional_index(
            close_data=self.candle[3],
            high_data=self.candle[1],
            low_data=self.candle[2],
            period=settings.ndi_period
        )

        self.ema_data = ema.exponential_moving_average(
            data=self.candle[3],
            period=settings.ema_periood
        )

        self.rsi_data = rsi.relative_strength_index(
            data=self.candle[3],
            period=settings.rsi_period
        )

        self.atr_data = atr.average_true_range(
            close_data=self.candle[3],
            period=settings.atr_period
        )
        self.on_screen_print()
        self.buy_logic()
        self.sell_logic()



'''
"command" : "", "order_id": 1, "symbol": "XBTUSD", "price" : "6445"
"command" : "EXIT_BUY", "order_id": 1, "symbol": "XBTUSD", "price" : "6445"
"command" : "SHORT", "order_id": 1, "symbol": "XBTUSD", "price" : "6445"
"command" : "EXIT_BUY", "order_id": 1, "symbol": "XBTUSD", "price" : "6445"
'''