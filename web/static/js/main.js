

function start() {
    /** JSON.stringify( $('form').serializeArray() ) */

    $.ajax({
        type: 'POST',
        url: '/rest/start',
        data: $('form').serialize(),
        dataType: 'json',
        statusCode: {
            401: function(data) {
                alert(data);
            },
            200: function (data) {
                alert(data);
            }
        }
    });
}

function stop() {
    $.ajax({
        type: 'POST',
        url: '/rest/stop',
        dataType: 'json',
        statusCode: {
            401: function(data) {
                alert(data);
            },
            200: function (data) {
                alert(data);
            }
        }
    });
}

function update() {
    $.ajax({
        type: 'POST',
        url: '/rest/update',
        dataType: 'json',
        data: $('form').serialize(),
        statusCode: {
            401: function(data) {
                alert(data);
            },
            200: function (data) {
                alert(data);
            }
        }
    });
}

function restart() {
    $.ajax({
        type: 'POST',
        url: '/rest/restart',
        dataType: 'json',
        statusCode: {
            401: function(data) {
                alert(data);
            },
            200: function (data) {
                alert(data);
            }
        }
    });
}

function send_signal() {
    $.ajax({
        type: 'POST',
        url: '/rest/test_consume',
        dataType: 'json',
        data: $('form').serialize(),
        statusCode: {
            401: function(data) {
                alert(data);
            },
            200: function (data) {
                alert(data);
            }
        }
    });
}

function drop_signal() {
$.ajax({
        type: 'POST',
        url: '/rest/test_drop_signal',
        dataType: 'json',
        data: $('form').serialize(),
        statusCode: {
            401: function(data) {
                alert(data);
            },
            200: function (data) {
                alert(data);
            }
        }
    });
}